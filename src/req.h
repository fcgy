/* Copyright (c) 2015-2016 Yoran Heling

  Permission is hereby granted, free of charge, to any person obtaining
  a copy of this software and associated documentation files (the
  "Software"), to deal in the Software without restriction, including
  without limitation the rights to use, copy, modify, merge, publish,
  distribute, sublicense, and/or sell copies of the Software, and to
  permit persons to whom the Software is furnished to do so, subject to
  the following conditions:

  The above copyright notice and this permission notice shall be included
  in all copies or substantial portions of the Software.

  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
  IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
  CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
  TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
  SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef FCGY_REQ_H
#define FCGY_REQ_H


typedef struct {
    uint32_t namelen,
             valuelen;
    char *buf;
} fcgy_req_param;


/* The front and back pointers also serve as a ref count. If both are NULL, the
 * request object is not referenced anywhere and may be freed. */
struct fcgy_req {
    fconn_fastcgi *front;
    uint16_t fconn_id;

    void *back;

    /* Order is unspecified for now */
    vec_t(fcgy_req_param) params;
};


fcgy_req *req_create(fconn_fastcgi *, uint16_t);

/* Lost connection with frontend */
void req_unset_front(fcgy_req *);

/* Set a parameter. Buffer follows the same format as given by
 * fastcgi_param_parse(). Ownership of the buffer is passed to the fcgy_req
 * object and will be free()'d. */
void req_set_param(fcgy_req *, uint32_t, uint32_t, char *);

#endif
